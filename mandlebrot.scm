#!/usr/bin/guile2.2 -s
!#


(define c 0.25+0.25i)
(define z c)

(define mandlebrot_calc_next
  (lambda (z c) (+ (* z z) c)))

(define is_unbounded
  (lambda (z)
    (> (+ (* (real-part z) (real-part z))
	  (* (imag-part z) (imag-part z)))
       4)))

(define max_iterations 50)
(define iter 0)

(while (< iter max_iterations)
  (if (is_unbounded z)
      ((display z)
       (display " found boundless at ")
       (display iter)
       (display "\n")
       (break #t))
      (set! z (mandlebrot_calc_next z c)))
  (set! iter (+ 1 iter)))
